
#ifndef  _OPS_CONFIG_H_
#define  _OPS_CONFIG_H_
#define  OPS_USE_HYPRE     0
#define  OPS_USE_INTEL_MKL 0
#define  OPS_USE_MATLAB    1
#define  OPS_USE_MEMWATCH  0
#define  OPS_USE_MPI       0
#define  OPS_USE_MUMPS     0
#define  OPS_USE_OMP       0
#define  OPS_USE_PHG       0
#define  OPS_USE_PETSC     0
#define  OPS_USE_SLEPC     0
#define  OPS_USE_UMFPACK   0
#define  PRINT_RANK    0

#if OPS_USE_MATLAB
#include	"bex/bex.h"
#endif

#endif  /* -- #ifndef _OPS_CONFIG_H_ -- */
