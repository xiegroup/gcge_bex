
#ifndef  _OPS_H_
#define  _OPS_H_

#include "ops_config.h"

#if OPS_USE_OMP
#include <omp.h>
#endif

#if OPS_USE_INTEL_MKL
#include <omp.h>
#include <mkl.h>
#include <mkl_spblas.h>
#endif

#if OPS_USE_MPI
#include <mpi.h>

extern double *debug_ptr;
baSize CreateMPIDataTypeSubMat(MPI_Datatype *submat_type,
	baSize nrows, baSize ncols, baSize ldA);
baSize DestroyMPIDataTypeSubMat(MPI_Datatype *submat_type);
baSize CreateMPIOpSubMatSum(MPI_Op *op);
baSize DestroyMPIOpSubMatSum(MPI_Op *op); 
#endif


typedef struct OPS_ {
	void   (*Printf) (const char *fmt, ...);
	double (*GetWtime) (void);
	baSize    (*GetOptionFromCommandLine) (
			const char *name, char type, void *data,
			baSize argc, char* argv[], struct OPS_ *ops);	  
	/* mat */
	void (*MatView) (void *mat, struct OPS_ *ops);  
	/* y = alpha x + beta y */
	void (*MatAxpby) (double alpha, void *matX, double beta, void *matY, struct OPS_ *ops);
	/* vec */
	void (*VecCreateByMat) (void **des_vec, void *src_mat, struct OPS_ *ops);
	void (*VecCreateByVec) (void **des_vec, void *src_vec, struct OPS_ *ops);
	void (*VecDestroy)     (void **des_vec,                struct OPS_ *ops);
	void (*VecView)           (void *x, 	                         struct OPS_ *ops);
	/* inner_prod = x'y */
	void (*VecInnerProd)      (void *x, void *y, double *inner_prod, struct OPS_ *ops);
	/* inner_prod = x'y for each proc */
	void (*VecLocalInnerProd) (void *x, void *y, double *inner_prod, struct OPS_ *ops);
	void (*VecSetRandomValue) (void *x,                              struct OPS_ *ops);
	/* y = alpha x + beta y */
	void (*VecAxpby)          (double alpha, void *x, double beta, void *y, struct OPS_ *ops);
	/* y = mat  * x */
	void (*MatDotVec)      (void *mat, void *x, void *y, struct OPS_ *ops);
	/* y = mat' * x */
	void (*MatTransDotVec) (void *mat, void *x, void *y, struct OPS_ *ops);
	/* multi-vec */
	void (*MultiVecCreateByMat)      (void ***multi_vec, baSize num_vec, void *src_mat, struct OPS_ *ops);
	void (*MultiVecCreateByVec)      (void ***multi_vec, baSize num_vec, void *src_vec, struct OPS_ *ops);
	void (*MultiVecCreateByMultiVec) (void ***multi_vec, baSize num_vec, void **src_mv, struct OPS_ *ops);
	void (*MultiVecDestroy)          (void ***multi_vec, baSize num_vec,                struct OPS_ *ops);
	/* *vec = multi_vec[col] */
	void (*GetVecFromMultiVec)    (void **multi_vec, baSize col, void **vec, struct OPS_ *ops);
	void (*RestoreVecForMultiVec) (void **multi_vec, baSize col, void **vec, struct OPS_ *ops);
	void (*MultiVecView)           (void **x, baSize start, baSize end, struct OPS_ *ops);
	void (*MultiVecLocalInnerProd) (char nsdIP, 
			void **x, void **y, baSize is_vec, baSize *start, baSize *end, 
			double *inner_prod, baSize ldIP, struct OPS_ *ops);
	void (*MultiVecInnerProd)      (char nsdIP, 
			void **x, void **y, baSize is_vec, baSize *start, baSize *end, 
			double *inner_prod, baSize ldIP, struct OPS_ *ops);
	void (*MultiVecSetRandomValue) (void **multi_vec, 
			baSize    start , baSize  end , struct OPS_ *ops);
	void (*MultiVecAxpby)          (
			double alpha , void **x , double beta, void **y, 
			baSize    *start, baSize  *end, struct OPS_ *ops);
	/* y = x coef + y diag(beta) */
	void (*MultiVecLinearComb)     (
			void   **x   , void **y , baSize is_vec, 
			baSize    *start, baSize  *end, 
			double *coef , baSize  ldc , 
			double *beta , baSize  incb, struct OPS_ *ops);
	void (*MatDotMultiVec)      (void *mat, void **x, void **y, 
			baSize  *start, baSize *end, struct OPS_ *ops);
	void (*MatTransDotMultiVec) (void *mat, void **x, void **y, 
			baSize  *start, baSize *end, struct OPS_ *ops);
	/* qAp = Qt A P */
	void (*MultiVecQtAP)        (char ntsA, char ntsdQAP, 
			void **mvQ  , void   *matA  , void   **mvP, baSize is_vec, 
			baSize  *start , baSize    *end   , double *qAp , baSize ldQAP , 
			void **mv_ws, struct OPS_ *ops);
	/* Dense matrix vector ops */ 
	struct OPS_ *lapack_ops; /* ���ܾ��������Ĳ��� */
	void (*DenseMatQtAP) (char ntluA, char nsdC,
			baSize nrowsA, baSize ncolsA, /* matA �������� */
			baSize nrowsC, baSize ncolsC, /* matC �������� */
			double alpha, double *matQ, baSize ldQ, 
		              double *matA, baSize ldA,
	                      double *matP, baSize ldP,
			double beta , double *matC, baSize ldC,
			double *dbl_ws);
	void (*DenseMatOrth) (double *mat, baSize nrows, baSize ldm, 
			baSize start, baSize *end, double orth_zero_tol,
			double *dbl_ws, baSize length, baSize *int_ws);
	/* linear solver */
	void (*LinearSolver)      (void *mat, void *b, void *x, 
			struct OPS_ *ops);
	void *linear_solver_workspace;
	void (*MultiLinearSolver) (void *mat, void **b, void **x, 
			baSize *start, baSize *end, struct OPS_ *ops); 
	void *multi_linear_solver_workspace;	
	/* orthonormal */
	void (*MultiVecOrth) (void **x, baSize start_x, baSize *end_x, 
			void *B, struct OPS_ *ops);
	void *orth_workspace;
	/* eigen solver */
	void (*EigenSolver) (void *A, void *B , double *eval, void **evec,
		baSize nevGiven, baSize *nevConv, struct OPS_ *ops);	
	void *eigen_solver_workspace;
	
	/* for pas */
	struct OPS_ *app_ops;
} OPS;

void OPS_Create  (OPS **ops);
void OPS_Setup   (OPS  *ops);
void OPS_Destroy (OPS **ops);

/* multi-vec */
void DefaultPrintf (const char *fmt, ...);
double DefaultGetWtime (void);
baSize  DefaultGetOptionFromCommandLine (
		const char *name, char type, void *value,
		baSize argc, char* argv[], struct OPS_ *ops);
void DefaultMultiVecCreateByVec      (void ***multi_vec, baSize num_vec, void *src_vec, struct OPS_ *ops);
void DefaultMultiVecCreateByMat      (void ***multi_vec, baSize num_vec, void *src_mat, struct OPS_ *ops);
void DefaultMultiVecCreateByMultiVec (void ***multi_vec, baSize num_vec, void **src_mv, struct OPS_ *ops);
void DefaultMultiVecDestroy          (void ***multi_vec, baSize num_vec, struct OPS_ *ops);
void DefaultGetVecFromMultiVec    (void **multi_vec, baSize col, void **vec, struct OPS_ *ops);
void DefaultRestoreVecForMultiVec (void **multi_vec, baSize col, void **vec, struct OPS_ *ops);
void DefaultMultiVecView           (void **x, baSize start, baSize end, struct OPS_ *ops);
void DefaultMultiVecLocalInnerProd (char nsdIP, void **x, void **y, baSize is_vec, baSize *start, baSize *end, 
	double *inner_prod, baSize ldIP, struct OPS_ *ops);
void DefaultMultiVecInnerProd      (char nsdIP, void **x, void **y, baSize is_vec, baSize *start, baSize *end, 
	double *inner_prod, baSize ldIP, struct OPS_ *ops);
void DefaultMultiVecSetRandomValue (void **x, baSize start, baSize end, struct OPS_ *ops);
void DefaultMultiVecAxpby          (
	double alpha , void **x , double beta, void **y, 
	baSize    *start, baSize  *end, struct OPS_ *ops);
void DefaultMultiVecLinearComb     (
	void   **x   , void **y , baSize is_vec, 
	baSize    *start, baSize  *end, 
	double *coef , baSize  ldc , 
	double *beta , baSize  incb, struct OPS_ *ops);
void DefaultMatDotMultiVec      (void *mat, void **x, void **y, 
	baSize  *start, baSize *end, struct OPS_ *ops);
void DefaultMatTransDotMultiVec (void *mat, void **x, void **y, 
	baSize  *start, baSize *end, struct OPS_ *ops);
void DefaultMultiVecQtAP        (char ntsA, char ntsdQAP, 
	void **mvQ   , void   *matA  , void   **mvP, baSize is_vec, 
	baSize  *startQP, baSize    *endQP , double *qAp , baSize ldQAP , 
	void **mv_ws, struct OPS_ *ops);

#endif  /* -- #ifndef _OPS_H_ -- */
