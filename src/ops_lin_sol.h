
#ifndef  _OPS_LIN_SOL_H_
#define  _OPS_LIN_SOL_H_

#include	"ops.h"
#include	"app_lapack.h"

typedef struct PCGSolver_ {
	baSize max_iter; double rate; double tol; char tol_type[8];
	void *vec_ws[3];  /* r p w */
	void *pc;
	baSize niter; double residual;
}PCGSolver;

void LinearSolverSetup_PCG(
	baSize max_iter, double rate, double tol, const char *tol_type, 
	void *vec_ws[3], void *pc, struct OPS_ *ops);
			
typedef struct BlockPCGSolver_ {
	baSize max_iter; double rate; double tol; char tol_type[8];
	void   **mv_ws[3]; /* r p w */
	double *dbl_ws; /* (6*length of vec) */
	baSize    *int_ws; /* (2*length of vec) */
	void   *pc;
	/* y = Ax in CG are all replaceed by the following ops 
	 * z is workspace from z[s]
	 * GCGE with shift will be implemented for computeW */
	void   (*MatDotMultiVec)(void **x, void **y, baSize *start, baSize *end, void **z, baSize s, struct OPS_ *ops);
	baSize niter; double residual;
}BlockPCGSolver;
void MultiLinearSolverSetup_BlockPCG(
	baSize max_iter, double rate, double tol, const char *tol_type,
	void   **mv_ws[3], double *dbl_ws, baSize *int_ws,
	void   *pc, void (*MatDotMultiVec)(void **x, void **y, baSize *start, baSize *end, void **z, baSize s, struct OPS_ *ops), 
	struct OPS_ *ops);


#endif  /* -- #ifndef _OPS_LIN_SOL_H_ -- */


