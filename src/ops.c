
#include	<stdio.h>
#include	<stdlib.h>
#include	<math.h>
#include	<assert.h>

#include    "ops.h"
#include    "app_lapack.h"

#define     DEBUG 0




void OPS_Create  (OPS **ops)
{
	*ops = malloc(sizeof(OPS));
	(*ops)->Printf                   = NULL;
	(*ops)->GetWtime                 = NULL;
	(*ops)->GetOptionFromCommandLine = NULL; 
	/* mat */
	(*ops)->MatAxpby                 = NULL;
	(*ops)->MatView                  = NULL;
	/* vec */
	(*ops)->VecCreateByMat           = NULL;
	(*ops)->VecCreateByVec           = NULL;
	(*ops)->VecDestroy               = NULL;
	(*ops)->VecView                  = NULL;
	(*ops)->VecInnerProd             = NULL;
	(*ops)->VecLocalInnerProd        = NULL;  
	(*ops)->VecSetRandomValue        = NULL;  
	(*ops)->VecAxpby                 = NULL;  
	(*ops)->MatDotVec                = NULL;  
	(*ops)->MatTransDotVec           = NULL;
	/* multi-vec */
	(*ops)->MultiVecCreateByMat      = NULL;  
	(*ops)->MultiVecCreateByVec      = NULL;  
	(*ops)->MultiVecCreateByMultiVec = NULL;  
	(*ops)->MultiVecDestroy          = NULL;  
	(*ops)->GetVecFromMultiVec       = NULL;  
	(*ops)->RestoreVecForMultiVec    = NULL;  
	(*ops)->MultiVecView             = NULL;  
	(*ops)->MultiVecLocalInnerProd   = NULL;  
	(*ops)->MultiVecInnerProd        = NULL;  
	(*ops)->MultiVecSetRandomValue   = NULL;  
	(*ops)->MultiVecAxpby            = NULL;  
	(*ops)->MultiVecLinearComb       = NULL;  
	(*ops)->MatDotMultiVec           = NULL; 
	(*ops)->MatTransDotMultiVec      = NULL; 
	(*ops)->MultiVecQtAP             = NULL;
	/* dense mat */
	(*ops)->lapack_ops               = NULL;
	(*ops)->DenseMatQtAP             = NULL;
	(*ops)->DenseMatOrth             = NULL;
	/* linear solver */
	(*ops)->LinearSolver             = NULL;		
	(*ops)->linear_solver_workspace  = NULL; 
	(*ops)->MultiLinearSolver        = NULL; 
	(*ops)->multi_linear_solver_workspace = NULL;
	/* orth */
	(*ops)->MultiVecOrth             = NULL; 
	(*ops)->orth_workspace           = NULL;
	/* eigen solver */
	(*ops)->EigenSolver              = NULL; 
	(*ops)->eigen_solver_workspace   = NULL; 
	/* app ops for pas */
	(*ops)->app_ops                  = NULL;
	return;
}
void OPS_Setup   (OPS  *ops)
{
	if (ops->Printf == NULL) {
		ops->Printf = DefaultPrintf;
	}
	if (ops->GetWtime == NULL) {
		ops->GetWtime = DefaultGetWtime;
	}
	if (ops->GetOptionFromCommandLine == NULL) {
		ops->GetOptionFromCommandLine = DefaultGetOptionFromCommandLine;
	}
	if (ops->lapack_ops == NULL) {
		OPS_Create (&(ops->lapack_ops));
		OPS_LAPACK_Set (ops->lapack_ops);
	}
	if (ops->DenseMatQtAP == NULL) {
		ops->DenseMatQtAP = ops->lapack_ops->DenseMatQtAP;
	}
	if (ops->DenseMatOrth == NULL) {
		ops->DenseMatOrth = ops->lapack_ops->DenseMatOrth;
	}

	if (ops->MultiVecInnerProd == NULL) {
		ops->MultiVecInnerProd = DefaultMultiVecInnerProd;
	}
	if (ops->MultiVecQtAP == NULL) {
		ops->MultiVecQtAP = DefaultMultiVecQtAP;
	}
	
	
	return;
}
void OPS_Destroy (OPS **ops)
{
	if ((*ops)->lapack_ops!=NULL) {
		OPS_Destroy(&((*ops)->lapack_ops));
	}
	free(*ops); *ops = NULL;
	return;
}
