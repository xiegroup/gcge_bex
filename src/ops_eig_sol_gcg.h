#ifndef  _OPS_EIG_SOL_GCG_H_
#define  _OPS_EIG_SOL_GCG_H_

#include	"ops.h"
#include    "ops_orth.h"
#include    "ops_lin_sol.h"
#include    "app_lapack.h"

typedef struct GCGSolver_ {
	void   *A        ; void  *B      ; double sigma;
	double *eval     ; void  **evec  ; 
	baSize    ifprint;
	baSize    nevMax    ; baSize   multiMax; double gapMin;
	baSize    nevInit   ; baSize   nevGiven; baSize    nevConv;
	baSize    block_size; double tol[2] ; baSize numIterMax; 
	baSize    numIter   ; baSize    sizeV  ;
	void   **mv_ws[4]; double *dbl_ws; baSize *int_ws;
	baSize    length_dbl_ws;
	baSize    user_defined_multi_linear_solver;	
	baSize  check_conv_max_num;
	char initX_orth_method[8] ; baSize    initX_orth_block_size; 
	baSize  initX_orth_max_reorth; double initX_orth_zero_tol;
	char compP_orth_method[8] ; baSize    compP_orth_block_size; 
	baSize  compP_orth_max_reorth; double compP_orth_zero_tol;
	char compW_orth_method[8] ; baSize    compW_orth_block_size; 
	baSize  compW_orth_max_reorth; double compW_orth_zero_tol;
	baSize  compW_cg_max_iter    ; double compW_cg_rate; 
	double compW_cg_tol       ; char   compW_cg_tol_type[8];
	baSize  compW_cg_auto_shift  ; double compW_cg_shift;
	baSize  compW_cg_order       ;
	baSize    compRR_min_num     ; double compRR_min_gap; 
	double compRR_tol; /*tol for dsyevx_ */	
} GCGSolver;

void EigenSolverSetup_GCG(
	baSize ifprint,
	baSize    multiMax, double gapMin, 
	baSize    nevInit , baSize    nevMax, baSize block_size,
	double tol[2]  , baSize    numIterMax,
	baSize    user_defined_multi_linear_solver,
	void **mv_ws[4], double *dbl_ws   , baSize *int_ws, 
	struct OPS_ *ops);
	
void EigenSolverCreateWorkspace_GCG(
	baSize nevInit, baSize nevMax, baSize block_size, void *mat,
	void ***mv_ws, double **dbl_ws, baSize **int_ws, 
	struct OPS_ *ops);

void EigenSolverDestroyWorkspace_GCG(
	baSize nevInit, baSize nevMax, baSize block_size, void *mat,
	void ***mv_ws, double **dbl_ws, baSize **int_ws, 
	struct OPS_ *ops);
	
void EigenSolverSetParameters_GCG(
	baSize    check_conv_max_num,
	const char *initX_orth_method, baSize initX_orth_block_size, baSize initX_orth_max_reorth, double initX_orth_zero_tol,
	const char *compP_orth_method, baSize compP_orth_block_size, baSize compP_orth_max_reorth, double compP_orth_zero_tol,
	const char *compW_orth_method, baSize compW_orth_block_size, baSize compW_orth_max_reorth, double compW_orth_zero_tol,
	baSize    compW_cg_max_iter , double compW_cg_rate, 
	double compW_cg_tol      , const char *compW_cg_tol_type, baSize compW_cg_auto_shift  ,
	baSize    compRR_min_num, double compRR_min_gap, double compRR_tol, 
	struct OPS_ *ops);

void EigenSolverSetParametersFromCommandLine_GCG(
	baSize argc, char* argv[], struct OPS_ *ops);
#endif  /* -- #ifndef _OPS_EIG_SOL_GCG_H_ -- */

