
#ifndef  _OPS_ORTH_H_
#define  _OPS_ORTH_H_

#include    "ops.h"
#include    "app_lapack.h"

typedef struct ModifiedGramSchmidtOrth_ {
	baSize    block_size;  
	baSize    max_reorth;
	double orth_zero_tol; 
	double reorth_tol;
	void   **mv_ws;      
	double *dbl_ws;   
} ModifiedGramSchmidtOrth;

typedef struct BinaryGramSchmidtOrth_ {
	baSize    block_size;   
	baSize    max_reorth;
	double orth_zero_tol; 
	double reorth_tol;
	void   **mv_ws;       
	double *dbl_ws;   
} BinaryGramSchmidtOrth;

void MultiVecOrthSetup_ModifiedGramSchmidt(
	baSize block_size, baSize max_reorth, double orth_zero_tol, 
	void **mv_ws, double *dbl_ws, struct OPS_ *ops);
void MultiVecOrthSetup_BinaryGramSchmidt(
	baSize block_size, baSize max_reorth, double orth_zero_tol, 
	void **mv_ws, double *dbl_ws, struct OPS_ *ops);

#endif  /* -- #ifndef _OPS_ORTH_H_ -- */


