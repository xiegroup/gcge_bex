
#ifndef  _APP_CCS_H_
#define  _APP_CCS_H_

#include	"ops.h"
#include	"app_lapack.h"

typedef struct CCSMAT_ {
	double *data ; 
	baSize    *i_row; baSize *j_col;
	baSize    nrows ; baSize ncols ;
} CCSMAT;


void OPS_CCS_Set  (struct OPS_ *ops);
	
#endif  
