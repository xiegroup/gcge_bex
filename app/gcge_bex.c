#include	<stdio.h>
#include	<stdlib.h>
#include	<assert.h>
#include  	<math.h>
#include   	<memory.h>
#include	<float.h>

#include	"ops.h"
#include    "ops_eig_sol_gcg.h"
#include	"app_lapack.h"
#include	"app_ccs.h"

#if OPS_USE_MATLAB
#include	"bex/bex.h"


#define DEBUG 0
/*
 * plhs:
 *   eval, evec, nevConv
 * prhs:
 *   A, B, nev, multiMax, gapMin, block_size, [abs_tol, rel_tol], numIterMax, maxcg
 *
 * TODO: use options to set other parameters
 */
void bexFunction(int nlhs, bxArray *plhs[], int nrhs, const bxArray *prhs[])
{
	printf("gcge for baltamatica : https://github.com/Materials-Of-Numerical-Algebra/GCGE/\n");

	OPS *ccs_ops = NULL;
	OPS_Create (&ccs_ops);
	OPS_CCS_Set (ccs_ops);
	OPS_Setup   (ccs_ops);
	CCSMAT A, *B;
	if (nrhs==0) {
		printf("-------------------------------\n");
		printf("Input Args (at least 2):\n");
		//printf("A, B, nev, abs_tol, rel_tol, nevMax, blockSize, nevInit, numIterMax, gapMin\n");
		printf("A\t\t: Ax = k Bx\tshould be sparse matrix\n");
		printf("B\t\t: Ax = k Bx\tshould be sparse matrix or []\n");
		printf("nev\t\t: The number of requested eigenpairs\n");
		printf("abs_tol\t\t: Absolute tolerance\n");
		printf("rel_tol\t\t: Relative tolerance\n");
		printf("nevMax\t: Maximum number of computed eigenpairs\n");
		printf("blockSize\t: The size of P and W\n");
		printf("nevInit\t\t: The size of working X\n");
		printf("numIterMax\t: Maximum number of iterations\n");
		printf("gapMin\t\t: The minimum relative gap of eigenvalues\n");
		printf("-------------------------------\n");
		printf("Output Args\n");
		printf("eval\t\t: eigenvalues\n");
		printf("evec\t\t: eigenvectors\n");
		printf("nevConv\t: The number of converged eigenpairs\n");
		printf("-------------------------------\n");
		printf("Bug report: zjwang@lsec.cc.ac.cn\n");
		return;		
	}
	if (nrhs < 1)
	{
		printf("Input at least one sparse matrix\n");
		return;
	}
	baSize ifprint, nevConv, nevMax, nevInit, block_size, numIterMax, maxcg;
	double tol[2], gapMin;
	nevConv = 10;
	nevMax = 2*nevConv; 
	block_size = nevConv<30?nevConv /  2:nevConv/5;
	nevInit = nevMax;
	numIterMax = 500;
	ifprint = 0;
	tol[0] = 1e-8;
	tol[1] = 1e-6;
	gapMin = 1e-5;
	maxcg = 9;

	if (nrhs < 2) {
		B = NULL;
		printf("Standard eigenvalue problem\n");
	}
	if (0==bxIsSparse(prhs[0])) 
	{
		printf("A should be a sparse matrix\n");
		return;
	}
	else {
		A.i_row = bxGetIr(prhs[0]); 
		A.j_col = bxGetJc(prhs[0]); 
		A.data  = bxGetPr(prhs[0]); 
		A.nrows = bxGetN (prhs[0]);
		A.ncols = A.nrows;
		// ccs_ops->Printf("A nrows = %d, ncols = %d\n", A.nrows, A.ncols);
		if (A.nrows < 80000) maxcg = 9;
		else if (A.nrows < 150000) maxcg = 11;
		else if (A.nrows < 300000) maxcg = 13;
		else maxcg = 30;
	}
	if (nrhs >= 2)
	{
		baSize M_prhs1 = bxGetM(prhs[1]);
		baSize N_prhs1 = bxGetN(prhs[1]);
		if (M_prhs1 * N_prhs1 > 1)
		{
			ccs_ops->Printf("Generalized eigenvalue problem\n");
			if (0==bxIsSparse(prhs[1])) {
				B = NULL;
				ccs_ops->Printf("B is a identity matrix\n");
			}
			else{
				B = malloc(sizeof(CCSMAT));
				B->i_row = bxGetIr(prhs[1]); 
				B->j_col = bxGetJc(prhs[1]); 
				B->data  = bxGetPr(prhs[1]); 
				B->nrows = bxGetN(prhs[1]);
				B->ncols = B->nrows;
				// ccs_ops->Printf("B nrows = %d, ncols = %d\n", B->nrows, B->ncols);
			}

			if (nrhs >= 3) 
			{	
				ifprint = *bxGetDoubles(prhs[2]);
				if (nrhs >= 4)
				{
					nevConv = *bxGetDoubles(prhs[3]);
					if (nevConv >= A.nrows) nevConv = A.nrows-1;
					if (nrhs == 4)
					{
						if (nevConv < 5) 
						{
							block_size = nevConv; 
							nevInit = 2 * nevConv;
							nevMax = 2 * nevConv;
						}
						else if (nevConv < 20)
						{
							block_size = nevConv / 2; 
							nevInit = 2 * nevConv;
							nevMax = 2 * nevConv;
						}
						else if (nevConv <= 200)
						{
							block_size = nevConv / 5;
							nevInit = nevConv;
							nevMax = nevInit + nevConv;
						}
						else if (nevConv < 800)
						{
							block_size = nevConv / 8;
							nevInit = 6 * block_size;
							nevMax = nevInit + nevConv;
						}
						else if (nevConv == 800)
						{
							block_size = 80;
							nevInit = 350;
							nevMax = 1350;
						}
						else
						{
							block_size = 200;
							nevInit = 3 * block_size;
							nevMax = nevConv + nevInit;
						}
					}
				}
			}
			if (nrhs >= 5) tol[0] =*bxGetDoubles(prhs[4]);
			if (nrhs >= 6) tol[1] = *bxGetDoubles(prhs[5]);
			
			if (nrhs >= 7) numIterMax = *bxGetDoubles(prhs[6]);
			
			if (nrhs >= 8) nevMax = *bxGetDoubles(prhs[7]); 
			if (nevMax > A.nrows) nevMax = A.nrows;

			if (nrhs >= 9) block_size = *bxGetDoubles(prhs[8]);
			block_size = block_size<nevMax?block_size:nevMax;
			
			if (nrhs >= 10) nevInit =*bxGetDoubles(prhs[9]);
			nevInit = nevInit<nevMax?nevInit:nevMax;

			if (nevInit<3*block_size) nevInit = nevMax;
			if (nrhs >= 11) gapMin = *bxGetDoubles(prhs[10]);
			if (nrhs >= 12) maxcg = *bxGetDoubles(prhs[11]);
		}
		else
		{
			B = NULL;
			ccs_ops->Printf("Standard eigenvalue problem\n");
			if (nrhs >= 2) 
			{	
				ifprint = *bxGetDoubles(prhs[1]);
				if (nrhs >= 3)
				{
					nevConv = *bxGetDoubles(prhs[2]);
					if (nevConv >= A.nrows) nevConv = A.nrows-1;
					if (nrhs == 3)
					{
						if (nevConv < 5) 
						{
							block_size = nevConv; 
							nevInit = 2 * nevConv;
							nevMax = 2 * nevConv;
						}
						else if (nevConv < 20)
						{
							block_size = nevConv / 2; 
							nevInit = 2 * nevConv;
								nevMax = 2 * nevConv;
						}
						else if (nevConv <= 200)
						{
							block_size = nevConv / 5;
							nevInit = nevConv;
							nevMax = nevInit + nevConv;
						}
						else if (nevConv < 800)
						{
							block_size = nevConv / 8;
							nevInit = 6 * block_size;
							nevMax = nevInit + nevConv;
						}
						else if (nevConv == 800)
						{
							block_size = 80;
							nevInit = 350;
							nevMax = 1350;
						}
						else
						{
							block_size = 200;
							nevInit = 3 * block_size;
							nevMax = nevConv + nevInit;
						}
					}
				}
			}
			
			if (nrhs >= 4) tol[0] =*bxGetDoubles(prhs[3]);
			if (nrhs >= 5) tol[1] = *bxGetDoubles(prhs[4]);
			
			if (nrhs >= 6) nevMax = *bxGetDoubles(prhs[5]);
			if (nevMax > A.nrows) nevMax = A.nrows;
			
			if (nrhs >= 7) block_size = *bxGetDoubles(prhs[6]);
			block_size = block_size<nevMax?block_size:nevMax;
			
			if (nrhs >= 8) nevInit =*bxGetDoubles(prhs[7]);
			nevInit = nevInit<nevMax?nevInit:nevMax;
			if (nevInit<3*block_size) nevInit = nevMax;
			if (nrhs >= 9) numIterMax = *bxGetDoubles(prhs[8]); 
			if (nrhs >= 10) gapMin = *bxGetDoubles(prhs[9]);
			if (nrhs >= 11) maxcg = *bxGetDoubles(prhs[10]);
		}
	}

	
	void **mv_ws[4]; double *dbl_ws = NULL; baSize *int_ws = NULL;

	baSize user_nev = nevConv;
	EigenSolverCreateWorkspace_GCG(nevInit,nevMax,block_size,(void *)(&A),
			mv_ws,&dbl_ws,&int_ws,ccs_ops);
	EigenSolverSetup_GCG(ifprint,-1,gapMin,nevInit,nevMax,block_size,
		tol,numIterMax,0,mv_ws,dbl_ws,int_ws,ccs_ops);
	baSize m = A.nrows, n = nevMax;

	baSize    check_conv_max_num    = 50;
	char   initX_orth_method[8]  = "mgs"; 
	baSize    initX_orth_block_size = -1  ; 
	baSize    initX_orth_max_reorth = 2   ; double initX_orth_zero_tol   = 2*DBL_EPSILON;
	char   compP_orth_method[8]  = "mgs"; 
	baSize    compP_orth_block_size = -1  ; 
	baSize    compP_orth_max_reorth = 2   ; double compP_orth_zero_tol   = 2*DBL_EPSILON;
	char   compW_orth_method[8]  = "mgs";
	baSize    compW_orth_block_size = -1  ; 	
	baSize    compW_orth_max_reorth = 2   ; double compW_orth_zero_tol   = 2*DBL_EPSILON;
	baSize    compW_bpcg_max_iter   = maxcg ; double compW_bpcg_rate       = 1e-2; 
	double compW_bpcg_tol        = 1e-12;char   compW_bpcg_tol_type[8]= "abs";
	baSize    compRR_min_num        = -1  ; double compRR_min_gap        = gapMin; 
	double compRR_tol            = 2*DBL_EPSILON;

			
	EigenSolverSetParameters_GCG(
			check_conv_max_num   ,
			initX_orth_method    , initX_orth_block_size, 
			initX_orth_max_reorth, initX_orth_zero_tol,
			compP_orth_method    , compP_orth_block_size, 
			compP_orth_max_reorth, compP_orth_zero_tol,
			compW_orth_method    , compW_orth_block_size, 
			compW_orth_max_reorth, compW_orth_zero_tol,
			compW_bpcg_max_iter  , compW_bpcg_rate, 
			compW_bpcg_tol       , compW_bpcg_tol_type, 1,
			compRR_min_num       , compRR_min_gap,
			compRR_tol           ,  
			ccs_ops);		
	if (nlhs <= 1 )	
	{
		plhs[0] = bxCreateDoubleMatrix(user_nev, 1, bxREAL);
		double *eval = (double *)malloc(n * 1 * sizeof(double));
		LAPACKVEC evec;
		evec.nrows = A.nrows; evec.ncols = nevMax; evec.ldd = A.nrows; 
		evec.data  = (double *)malloc(m*n*sizeof(double));
		ccs_ops->EigenSolver((void *)(&A),(void *)B,eval,(void **)(&evec),0,&nevConv,ccs_ops);
		double *out_eval;
		out_eval = bxGetPr(plhs[0]);
		memcpy(out_eval, eval, user_nev * sizeof(double));
	}
	if (nlhs == 2)
	{
		plhs[0] = bxCreateDoubleMatrix(user_nev, 1, bxREAL);
		plhs[1] = bxCreateDoubleMatrix(m, user_nev, bxREAL);
		double *eval = (double *)malloc(n * 1 * sizeof(double));
		LAPACKVEC evec;
		evec.nrows = A.nrows; evec.ncols = nevMax; evec.ldd = A.nrows; 
		evec.data  = (double *)malloc(m*n*sizeof(double));
		double *out_evec, *out_eval;
		out_evec = bxGetPr(plhs[1]);
		out_eval = bxGetPr(plhs[0]);
		ccs_ops->EigenSolver((void *)(&A),(void *)B,eval,(void **)(&evec),0,&nevConv,ccs_ops);
		memcpy(out_eval, eval, user_nev * sizeof(double));
		memcpy(out_evec, evec.data, user_nev * m * sizeof(double));

	}
	
	EigenSolverDestroyWorkspace_GCG(nevInit,nevMax,block_size,(void *)(&A),
			mv_ws,&dbl_ws,&int_ws,ccs_ops);
	OPS_Destroy (&ccs_ops);
	return;
}

#endif
