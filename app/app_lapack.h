/**
 *    @file  app_lapack.h
 *   @brief  app of lapack 
 *
 *  lapack�Ĳ����ӿ�, ��Գ��ܾ���
 *
 *  @author  Yu Li, liyu@tjufe.edu.cn
 *
 *       Created:  2020/8/13
 *      Revision:  none
 */
#ifndef  _APP_LAPACK_H_
#define  _APP_LAPACK_H_

#include	"ops.h"

#define FORTRAN_WRAPPER(x) x ## _

typedef struct LAPACKMAT_ {
	double *data; baSize nrows; baSize ncols; baSize ldd;
} LAPACKMAT;
typedef LAPACKMAT LAPACKVEC;

void OPS_LAPACK_Set  (struct OPS_ *ops);

#define dasum FORTRAN_WRAPPER(dasum)
#define daxpy FORTRAN_WRAPPER(daxpy)
#define dcopy FORTRAN_WRAPPER(dcopy)
#define ddot FORTRAN_WRAPPER(ddot)
#define dgemm FORTRAN_WRAPPER(dgemm)
#define dgemv FORTRAN_WRAPPER(dgemv)
#define dlamch FORTRAN_WRAPPER(dlamch)
#define idamax FORTRAN_WRAPPER(idamax)
#define dscal FORTRAN_WRAPPER(dscal)
#define dsymm FORTRAN_WRAPPER(dsymm)
#define dsymv FORTRAN_WRAPPER(dsymv)
#define dgeqp3 FORTRAN_WRAPPER(dgeqp3)
#define dorgqr FORTRAN_WRAPPER(dorgqr)
#define dgerqf FORTRAN_WRAPPER(dgerqf)
#define dorgrq FORTRAN_WRAPPER(dorgrq)
#define dsyev FORTRAN_WRAPPER(dsyev)
#define dsyevx FORTRAN_WRAPPER(dsyevx)
// 
// #if !OPS_USE_INTEL_MKL
// /* BLAS */
double dasum(baSize *n, double *dx, baSize *incx);
baSize daxpy(baSize *n, double *da, double *dx, baSize *incx, double *dy, baSize *incy);
baSize dcopy(baSize *n, double *dx, baSize *incx, double *dy, baSize *incy);
double ddot(baSize *n, double *dx, baSize *incx, double *dy, baSize *incy);
baSize dgemm(char *transa, char *transb, baSize *m, baSize *n, baSize *k,
		double *alpha, double *a, baSize *lda, 
		               double *b, baSize *ldb,
		double *beta , double *c, baSize *ldc);
baSize dgemv(char *trans, baSize *m, baSize *n,
		double *alpha, double *a, baSize *lda,
		               double *x, baSize *incx,
		double *beta , double *y, baSize *incy);
double dlamch(char *cmach);			
baSize idamax(baSize  *n, double *dx, baSize *incx);
baSize dscal(baSize *n, double *da, double *dx, baSize *incx);
baSize dsymm(char *side, char *uplo, baSize *m, baSize *n,
		double *alpha, double *a, baSize *lda, 
	 	               double *b, baSize *ldb, 
		double *beta , double *c, baSize *ldc);
baSize dsymv(char *uplo, baSize *n, 
		double *alpha, double *a, baSize *lda, 
		               double *x, baSize *incx, 
		double *beta , double *y, baSize *incy);
/* LAPACK */
/* DGEQP3 computes a QR factorization with column pivoting of 
 * a matrix A:  A*P = Q*R  using Level 3 BLAS 
 * LWORK >= 2*N+( N+1 )*NB, where NB is the optimal blocksize */
baSize dgeqp3(baSize *m, baSize *n, double *a, baSize *lda, baSize *jpvt,
	double *tau, double *work, baSize *lwork, baSize *info);
/* DORGQR generates an M-by-N real matrix Q with 
 * orthonormal columns 
 * K is the number of elementary reflectors whose product 
 * defines the matrix Q. N >= K >= 0.
 * LWORK >= N*NB, where NB is the optimal blocksize */
baSize dorgqr(baSize *m, baSize *n, baSize *k, double *a, baSize *lda,
	double *tau, double *work, baSize *lwork, baSize *info);
/* The length of the array WORK.  LWORK >= 1, when N <= 1;
 * otherwise 8*N.
 * For optimal efficiency, LWORK >= (NB+3)*N,
 * where NB is the max of the blocksize for DSYTRD and DORMTR
 * returned by ILAENV. */
/* RQ factorization */
baSize dgerqf(baSize *m, baSize *n, double *a, baSize *lda, 
	double *tau, double *work, baSize *lwork, baSize *info);
baSize dorgrq(baSize *m, baSize *n, baSize *k, double *a, baSize *lda, 
	double *tau, double *work, baSize *lwork, baSize *info);
baSize dsyev(char *jobz, char *uplo, baSize *n, 
	double *a, baSize *lda, double *w, 
	double *work, baSize *lwork, baSize *info);
baSize dsyevx(char *jobz, char *range, char *uplo, baSize *n, 
	double *a, baSize *lda, double *vl, double *vu, baSize *il, baSize *iu, 
	double *abstol, baSize *m, double *w, double *z, baSize *ldz, 
	double *work, baSize *lwork, baSize *iwork, baSize *ifail, baSize *info);
// #endif
// 	
#endif  /* -- #ifndef _APP_LAPACK_H_ -- */
