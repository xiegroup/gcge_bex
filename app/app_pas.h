/**
 *    @file  app_pas.h
 *   @brief  PAS ����ֵ����� 
 *
 *  PAS ����ֵ����� 
 *
 *  @author  Yu Li, liyu@tjufe.edu.cn
 *
 *       Created:  2020/8/29
 *      Revision:  none
 */
#ifndef  _APP_PAS_H_
#define  _APP_PAS_H_

#include    "ops.h"
#include    "app_lapack.h"

typedef struct PASMAT_ {
	void *XX  ; /* LAPACKMAT */
	void ***QX;
	void **QQ ; void **P;
	baSize  num_levels; baSize level_aux;
	baSize  size_XX;
} PASMAT;
typedef struct PASVEC_ {
	void **x ; /* LAPACKVEC */
	void ***q;
	void **P ; 
	baSize  num_levels; baSize level_aux;
	baSize  size_x;
} PASVEC;

void OPS_PAS_Set (struct OPS_ *ops, struct OPS_ *app_ops);

#endif   /* -- #ifndef _APP_PAS_H_ -- */
