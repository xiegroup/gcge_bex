# GCGE_BEX



Enter file GCGE-BEX/test and perform the following command, you can get the executable function "gcge_bex.bexw64" under Windows system, or "gcge_bex.bexa64" under Ubuntu system.

```bash
makefile_baltam
```

"testgcge.m" in folder GCGE-BEX/test/ is a test file for gcge_bex.
