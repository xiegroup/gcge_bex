% This is just an example to test gcge, lobpcg and eigs!!
% email--zjwang@lsec.cc.ac.cn
% Warning: 
% If you can't use the libraries directly, please mex the code again.
% The bex file(makefile_baltam) works on windows systems.
% Original Link: https://github.com/Materials-Of-Numerical-Algebra/GCGE
%%

%produce sparse matrix 
clear
load('Si5H12shift.mat');

%%
%gcge
tic;
D=gcge_bex(A);
% ifprint = 1;
% nev=10;
%[D, V] = gcge_bex(A,ifprint,nev,1e-8);
%[D, V] = gcge_bex(A,ifprint,nev,1e-8,1e-6); 
%[D, V] = gcge_bex(A,ifprint,nev,1e-8,1e-6,200);
toc
%%
% EIGS 
%会闪退
%nev=10;
%tic;meval=eigs(A,nev,'smallestabs','Tolerance',1e-8);toc/
